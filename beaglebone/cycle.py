#!/usr/bin/env python

import rgbled
import time

RED = (255, 0, 0)
BLUE = (0, 255, 0)
GREEN = (0, 0, 255)
CYAN = (0, 255, 255)
YELLOW = (255, 0, 255)
VIOLET = (255, 255, 0)
WHITE = (255, 255, 255)

led = rgbled.Led()

while True:
    led.setColorRGB(*RED)
    time.sleep(2)

    led.setColorRGB(*BLUE)
    time.sleep(2)

    led.setColorRGB(*GREEN)
    time.sleep(2)

    led.setColorRGB(*CYAN)
    time.sleep(2)

    led.setColorRGB(*YELLOW)
    time.sleep(2)

    led.setColorRGB(*VIOLET)
    time.sleep(2)

    led.setColorRGB(*WHITE)
    time.sleep(2)
