import bottle
import rgbled

@bottle.route("/hello")
def hello():
    return "Hello, webserver!"

form = """
       <p>Pick a color:</p>
       <form method="post">
           <input type="submit" name="color" value="Red"/>
           <input type="submit" name="color" value="Blue"/>
           <input type="submit" name="color" value="Green"/>
       </form>
"""

led = rgbled.Led()

@bottle.route("/control/<tag>")
def control_get(tag):
    return form

@bottle.post("/control/<tag>")
def control_post(tag):
    print(bottle.request.body.read())

    color = bottle.request.forms.get("color")

    if color == "Red":
        led.setColorRGB(255, 0, 0)
    elif color == "Green":
        led.setColorRGB(0, 255, 0)
    elif color == "Blue":
        led.setColorRGB(0, 0, 255)
    else:
        print("error: invalid color")

    return form

bottle.run(host='0.0.0.0', port=8081, debug=True)
