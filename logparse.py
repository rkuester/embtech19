def to_milliseconds(time):
    h, m, s = time.split(':')
    ms = 60 * 60 * 1000 * int(h)
    ms += 60 * 1000 * int(m)
    ms += 1000 * float(s)
    return ms

def format_time(ms):
    return '{:>10.3f}'.format(ms)

log = open('log.txt')
last_ms = 0

for line in log:
    time, rest = line.split(' ', 1)
    ms = to_milliseconds(time)
    delta = ms - last_ms
    last_ms = ms
    print(format_time(delta), rest.strip())
