# lora-rx main.py

import pycom
import time
from network import LoRa
import socket
import machine

pycom.heartbeat(False)

lora = LoRa(mode=LoRa.LORA, region=LoRa.US915)
sock = socket.socket(socket.AF_LORA, socket.SOCK_RAW)
sock.setblocking(True)

def ping_led():
    pycom.rgbled(0x00ff00)  # green
    time.sleep(.3)
    pycom.rgbled(0)


while True:
    data = sock.recv(64)
    ping_led()
    print(data)
