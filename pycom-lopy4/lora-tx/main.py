# lora-tx main.py

import pycom
import time
from network import LoRa
import socket
import machine

pycom.heartbeat(False)

lora = LoRa(mode=LoRa.LORA, region=LoRa.US915)
sock = socket.socket(socket.AF_LORA, socket.SOCK_RAW)

def ping_led():
    pycom.rgbled(0xff0000)  # red
    time.sleep(.3)
    pycom.rgbled(0)


while True:
    ping_led()
    sock.send("Hello, LoRa!")
    time.sleep(2 + machine.rng() % 5)
