# lora rgb test

import pycom
import time

pycom.heartbeat(False)

while True:
    pycom.rgbled(0x100000)  # Red
    time.sleep(.3)
    pycom.rgbled(0x001000)  # Green
    time.sleep(.3)
    pycom.rgbled(0x000010)  # Blue
    time.sleep(.3)
